##
## Makefile for Midgard in /home/jon/Workspace/ETNA/Midgard
## 
## Made by Manh-Tuong NGUYEN
## Login   <nguyen_m@etna-alternance.net>
## 
## Started on  Wed Dec  7 15:36:48 2016 Manh-Tuong NGUYEN
## Last update Thu Dec  8 11:20:32 2016 Manh-Tuong NGUYEN
##

MAKE	= @make -s

ECHO	= @echo -e "\e[0;32;1m"
NCOLOR	= @echo -ne "\e[0m"

all: intro battle final

intro:
	$(ECHO) Compiling introduction...
	$(NCOLOR)
	$(MAKE) -C introduction/

battle:
	$(ECHO) Compiling battle...
	$(NCOLOR)
	$(MAKE) -C battle/

final:
	$(ECHO) Compiling final...
	$(NCOLOR)
	$(MAKE) -C final/

clean:
	$(MAKE) -C introduction/ clean
	$(MAKE) -C battle/ clean
	$(MAKE) -C final/ clean

fclean: clean
	$(MAKE) -C introduction/ fclean
	$(MAKE) -C battle/ fclean
	$(MAKE) -C final/ fclean

re: fclean all

.PHONY: all intro battle final clean fclean re
