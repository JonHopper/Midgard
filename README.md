﻿Midgard
===

#### Langage:
**C**

#### Description:
Mini-RPG développé dans le cadre du cursus ETNA

#### Organisation:
* **src**
    * Sources (.c)
* **include**
    * Headers (.h)
* **lib**
    * LibMy
* **Makefile** (Makefile Hopper v3)
    * Couleurs
    * Compilation détaillée
    * Dépendance header auto-générée