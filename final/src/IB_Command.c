/*
** IB_Command.c for Midgard
** 
** Made by NOMENTSOA Cedric
** Login   <noment_c@etna-alternance.net>
** 
** Started on  Tue Dec  6 14:15:55 2016 NOMENTSOA Cedric
** Last update Fri Dec  9 08:56:39 2016 NOMENTSOA Cedric
*/

#include <stdlib.h>
#include <time.h>

#include "Bowser.h"
#include "libmy.h"
#include "Creature.h"
#include "Game.h"
#include "B_Command.h"

int	magic_catch()
{
  int	rnd;

  if (inventory_has(IT_MAGICBOX) == NULL)
    {
      my_putstr("Vous n'avez plus de magic box\n");
      return (1);
    }
  inventory_delete(IT_MAGICBOX, 1);
  if ((rnd = rand() % 3) < 1)
    {
      creature_info();
      if (team_add_creature(creature_dup()))
	return (1);
      my_putstr("Créature rajouté à votre team\n");
      creature_free();
      game_setstate(GS_OUTOFBATTLE);
    }
  else
    {
      if (fail_catch())
	return (1);
    }
  return (0);
}

int	help_me()
{
  if (bowser_get()->_idfight != -1)
    {
      my_putstr("Commande interdite\n");
      return (1);
    }
  my_putstr("Vous fuyez lachement\n");
  creature_free();
  return (creature_spawn());
}

int	check_creature()
{
  if (choose_creature()->pv <= 0)
    {
      my_putstr("Votre créature est morte\n");
      team_delete_creature(choose_creature());
      game_setstate(GS_OUTOFBATTLE);
      return (1);
    }
  return (0);
}

int	check_creature_ennemy()
{
  int rnd;

  if (creature_get()->pv <= 0)
    {
      rnd = (rand() % 31) + 90;
      my_putstr("Créature tué\nVous gagnez ");
      my_put_nbr(rnd);
      my_putstr(" Rupees\n");
      bowser_get()->_inventory->_pouch += rnd;
      creature_free();
      game_setstate(GS_OUTOFBATTLE);
      return (1);
    }
  return (0);
}

void	ennemy_attack()
{
  int	rnd;

  rnd = rand() % 4;
  if (rnd == 0)
    {
      my_putstr("L'ennemi vous slash : 15 points de dégats\n");
      choose_creature()->pv -= 15;
    }
  else if (rnd == 1)
    {
      my_putstr("L'ennemi vous fire : 30 points de dégats\n");
      choose_creature()->pv -= 30;
    }
  else if (rnd == 2)
    {
      my_putstr("L'ennemi gamble\n");
      gamble();
    }
  else
    {
      my_putstr("L'ennemi se repose\n");
      creature_get()->pm += 10;
    }
}
