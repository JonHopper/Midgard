/*
** Game.c for Midgard$ in /home/jon/Workspace/ETNA/Midgard/battle
** 
** Made by Manh-Tuong NGUYEN
** Login   <nguyen_m@etna-alternance.net>
** 
** Started on  Wed Dec  7 15:47:05 2016 Manh-Tuong NGUYEN
** Last update Thu Dec  8 19:17:12 2016 NOMENTSOA Cedric
*/

#include <stdlib.h>
#include <unistd.h>

#include "libmy.h"
#include "Bowser.h"
#include "Game.h"

static t_gamestate	game_state = GS_OUTOFBATTLE;

char		*readLine()
{
  ssize_t	ret;
  char		*buff;

  if ((buff = malloc(sizeof(*buff) * (50 + 1))) == NULL)
    return (NULL);
  if ((ret = read(0, buff, 50)) > -1)
    {
      if (ret > 0)
	buff[ret - 1] = '\0';
      else
	buff[0] = '\0';
      return (buff);
    }
  free(buff);
  return (NULL);
}

void		party_loop()
{
  char		*entry;
  t_bowser	*mr_b;

  mr_b = bowser_get();
  while (mr_b->_partyloop)
    {
      my_putstr("Votre tour> ");
      if ((entry = readLine()) == NULL || my_strlen(entry) == 0)
	return ;
      if (my_strcmp(entry, "quit") == 0 && game_getstate() == GS_OUTOFBATTLE)
	{
	  free(entry);
	  break ;
	}
      execute_command(entry);
      free(entry);
    }
}

void		execute_command(char const *cmd)
{
  t_command	*buff;

  buff = bowser_get()->_cmdlist;
  while (buff != NULL &&  my_strcmp(buff->_cmd, cmd))
    buff = buff->_next;
  if (buff == NULL || buff->_state != game_state)
    my_putstr("Commande inconnue\n");
  else
    buff->_func();
}

t_gamestate	game_getstate()
{
  return (game_state);
}

void		game_setstate(t_gamestate state)
{
  game_state = state;
  if (state == GS_INBATTLE)
    my_putstr("Mode bataille : ON\n");
  else
    my_putstr("Mode bataille : OFF\n");
}
