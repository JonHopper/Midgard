/*
** Creature.c for Midgard in /home/jon/Workspace/ETNA/Midgard/introduction
** 
** Made by Manh-Tuong NGUYEN
** Login   <nguyen_m@etna-alternance.net>
** 
** Started on  Tue Dec  6 13:12:01 2016 Manh-Tuong NGUYEN
** Last update Thu Dec  8 22:12:25 2016 NOMENTSOA Cedric
*/

#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "libmy.h"
#include "Creature.h"
#include "Bowser.h"

# define NBCREA	5

static t_creature g_creatures[] =
  {
    {"Koopa", 1, 10, 10, 20, 20},
    {"Bob bomb", 1, 10, 10, 20, 20},
    {"Yoshi", 1, 10, 10, 20, 20},
    {"Maskas", 1, 10, 10, 20, 20},
    {"Kucco", 1, 10, 10, 20, 20},
    {NULL, 0, 0, 0, 0, 0}
  };

static	t_creature	*mob_creature = NULL;

int		creature_spawn()
{
  int		rnd;

  if (mob_creature != NULL)
    return (1);
  rnd = rand() % NBCREA;
  if ((mob_creature = malloc(sizeof(t_creature))) == NULL)
    return (1);
  mob_creature->name = my_strdup(g_creatures[rnd].name);
  if (!mob_creature->name)
    return (1);
  mob_creature->lvl = g_creatures[rnd].lvl;
  mob_creature->pv = g_creatures[rnd].pv;
  mob_creature->pvmax = g_creatures[rnd].pvmax;
  mob_creature->pm = g_creatures[rnd].pm;
  mob_creature->pmmax = g_creatures[rnd].pmmax;
  my_putstr("Un ");
  my_putstr(mob_creature->name);
  my_putstr(" a pop !\n");
  return (0);
}

t_creature	*creature_dup()
{
  t_creature	*crea;

  if (mob_creature == NULL)
    return (NULL);
  if ((crea = malloc(sizeof(t_creature))) == NULL)
    return (NULL);
  crea->name = my_strdup(mob_creature->name);
  if (!crea->name)
    return (NULL);
  crea->lvl = mob_creature->lvl;
  crea->pv = mob_creature->pvmax;
  crea->pvmax = mob_creature->pvmax;
  crea->pm = mob_creature->pmmax;
  crea->pmmax = mob_creature->pmmax;
  return (crea);
}

t_creature	*creature_get()
{
  return (mob_creature);
}

void		creature_info()
{
  my_putstr(mob_creature->name);
  my_putstr(" capturé\nNiveau : ");
  my_put_nbr(mob_creature->lvl);
  my_putstr("\nPV : ");
  my_put_nbr(mob_creature->pvmax * 2);
  my_putstr("\nPM : ");
  my_put_nbr(mob_creature->pmmax * 2);
  my_putstr("\n");
}

void		creature_free()
{
  if (mob_creature == NULL)
    return ;
  free(mob_creature->name);
  free(mob_creature);
  mob_creature = NULL;
}
