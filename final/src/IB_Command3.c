/*
** IB_Command3.c for IB_Command3.C in /home/cedric/Documents/midgard/final/src
** 
** Made by NOMENTSOA Cedric
** Login   <noment_c@etna-alternance.net>
** 
** Started on  Thu Dec  8 22:14:28 2016 NOMENTSOA Cedric
** Last update Fri Dec  9 09:20:53 2016 NOMENTSOA Cedric
*/

#include <time.h>
#include <stdlib.h>

#include "libmy.h"
#include "B_Command.h"
#include "Creature.h"
#include "Bowser.h"
#include "Inventory.h"

int	fail_catch()
{
   my_putstr("Capture raté\n");
   if (bowser_get()->_idfight != -1)
     {
       ennemy_attack();
       if (check_creature())
	 return (1);
     }
   else
     {
       my_putstr("La créature vous attaque\nVous fuyez lachement\n");
       creature_free();
       return (creature_spawn());
     }
   return (0);
}

int	use_mushroom()
{
  int	rnd;

  if (bowser_get()->_idfight == -1)
    {
      my_putstr("Veuillez selectionner une créature à soigner\n");
      return (1);
    }
  if ((inventory_has(IT_MUSHROOM) == NULL))
    {
      my_putstr("Vous n'avez pas de champignons\n");
      return (1);
    }
  rnd = (rand() % 16) + 10;
  choose_creature()->pv -= rnd;
  inventory_delete(IT_MUSHROOM, 1);
  return (0);
}
