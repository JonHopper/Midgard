/*
** main.c for Midgard in /home/jon/Workspace/ETNA/Midgard/introduction/src
** 
** Made by Manh-Tuong NGUYEN
** Login   <nguyen_m@etna-alternance.net>
** 
** Started on  Tue Dec  6 09:46:18 2016 Manh-Tuong NGUYEN
** Last update Wed Dec  7 15:57:36 2016 Manh-Tuong NGUYEN
*/

#include <stdlib.h>
#include <time.h>

#include "libmy.h"
#include "Bowser.h"
#include "Arg.h"
#include "Game.h"

int	main(int argc, char *argv[])
{
  srand(time(NULL));
  if (catch_arg(argc, argv))
    return (1);
  if (bowser_init(argv[2]))
    return (1);
  if (command_init())
    return (1);
  party_loop();
  bowser_free();
  return (0);
}
