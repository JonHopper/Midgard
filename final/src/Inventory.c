/*
** Inventory.c for Midgard in /home/jon/Workspace/ETNA/Midgard/introduction
** 
** Made by Manh-Tuong NGUYEN
** Login   <nguyen_m@etna-alternance.net>
** 
** Started on  Tue Dec  6 13:35:01 2016 Manh-Tuong NGUYEN
** Last update Thu Dec  8 21:45:20 2016 NOMENTSOA Cedric
*/
#include <stdlib.h>

#include "Bowser.h"
#include "Inventory.h"

int		inventory_init()
{
  t_bowser	*mr_b;

  if ((mr_b = bowser_get()) == NULL)
    return (1);
  if ((mr_b->_inventory = malloc(sizeof(t_inventory))) == NULL)
    return (1);
  mr_b->_inventory->_pouch = 420;
  mr_b->_inventory->_itemlist = NULL;
  inventory_add(IT_MAGICBOX, 5);
  return (0);
}

t_itemnode	*inventory_has(t_item item)
{
  t_itemnode	*buff;

  buff = bowser_get()->_inventory->_itemlist;
  while (buff)
    {
      if (buff->_item == item)
	return (buff);
      buff = buff->_next;
    }
  return (NULL);
}

int		inventory_add(t_item item, unsigned int nb)
{
  t_bowser	*mr_b;
  t_itemnode	*itemnode;

  if ((mr_b = bowser_get()) == NULL)
    return (1);
  if ((itemnode = inventory_has(item)) != NULL)
    {
      itemnode->_nb += nb;
      return (0);
    }
  if ((itemnode = malloc(sizeof(t_itemnode))) == NULL)
    return (1);
  itemnode->_item = item;
  itemnode->_nb = nb;
  itemnode->_prev = NULL;
  itemnode->_next = mr_b->_inventory->_itemlist;
  mr_b->_inventory->_itemlist = itemnode;
  return (0);
}

void		inventory_delete(t_item item, unsigned int nb)
{
  t_bowser	*mr_b;
  t_itemnode	*itemnode;

  if ((mr_b = bowser_get()) == NULL)
    return ;
  if ((itemnode = inventory_has(item)) == NULL)
    return ;
  if (itemnode->_nb > nb)
    itemnode->_nb = 0;
  else
    itemnode->_nb -= nb;
}

void		inventory_free()
{
  t_bowser	*mr_b;
  t_itemnode	*buff;

  if ((mr_b = bowser_get()) == NULL || mr_b->_inventory == NULL)
    return ;
  while (mr_b->_inventory->_itemlist)
    {
      buff = mr_b->_inventory->_itemlist->_next;
      free(mr_b->_inventory->_itemlist);
      mr_b->_inventory->_itemlist = buff;
    }
  free(mr_b->_inventory);
  mr_b->_inventory = NULL;
}
