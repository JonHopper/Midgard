/*
** OoB_Command.c for Midgard in /home/jon/Workspace/ETNA/Midgard/battle
** 
** Made by Manh-Tuong NGUYEN
** Login   <nguyen_m@etna-alternance.net>
** 
** Started on  Wed Dec  7 16:45:31 2016 Manh-Tuong NGUYEN
** Last update Fri Dec  9 09:10:16 2016 NOMENTSOA Cedric
*/

#include <stdlib.h>

#include "libmy.h"
#include "Bowser.h"
#include "Game.h"
#include "Creature.h"
#include "B_Command.h"
#include "Inventory.h"

int		team_info()
{
  t_team	*buff;

  buff = bowser_get()->_team;
  if (buff == NULL)
    {
      my_putstr("Team vide\n");
      return (0);
    }
  while (buff)
    {
       my_putstr(buff->_creature->name);
       my_putstr(" n°");
       my_put_nbr(buff->_id);
       my_putstr("\nNiveau : ");
       my_put_nbr(buff->_creature->lvl);
       my_putstr("\nPV : ");
       my_put_nbr(buff->_creature->pvmax);
       my_putstr("\nPM : ");
       my_put_nbr(buff->_creature->pmmax);
       my_putstr("\n");
       buff = buff->_next;
    }
  return (0);
}

int		choose_one()
{
  char		*entry;
  t_team	*team;

  if (bowser_get()->_team == NULL)
    {
      my_putstr("Team vide\n");
      return (1);
    }
  team_info();
  my_putstr("Rentrez l'id de votre créature qui va combattre\n");
  while (1)
    {
      team = bowser_get()->_team;
      if ((entry = readLine()) == NULL || my_strlen(entry) == 0)
	break ;
      while (team != NULL && team->_id != my_getnbr(entry))
	team = team->_next;
      if (team == NULL)
	my_putstr("Créature inconnue\n");
      else
	break ;
    }
  bowser_get()->_idfight = my_getnbr(entry);
  free(entry);
  return (0);
}


int	fight()
{
  if (bowser_get()->_idfight == -1 && bowser_get()->_team_size > 0)
    {
      my_putstr("Vous avez des créatures, selectionnez en une\n");
      return (1);
    }
  if (creature_spawn())
    return (1);
  game_setstate(GS_INBATTLE);
  if (bowser_get()->_idfight != -1)
    {
      my_putstr("Votre ");
      my_putstr(choose_creature()->name);
      my_putstr(" a été lancé dans la bataille\n");
    }
  return (0);
}

int	buy_item()
{
  char	*entry;

  my_putstr("Vous avez ");
  my_put_nbr(bowser_get()->_inventory->_pouch);
  my_putstr(" Rupees\n");
  my_putstr("Champignons : 30 Rupees\nMagic Box : 90 Rupees\n");
  my_putstr("Que voulez vous acheter ?\n");
  while (1)
    {
      if ((entry = readLine()) == NULL || my_strlen(entry) == 0)
	break ;
      if (my_strcmp(entry, "Champignons") && my_strcmp(entry, "Magic Box"))
	{
	  my_putstr("Objets inconnus\n");
	  break ;
	}
      buy_item_inventory(entry);
      break ;
    }
  free(entry);
  return (0);
}

void	buy_item_inventory(char *entry)
{
  if (my_strcmp(entry, "Champignons") == 0)
    {
      if (bowser_get()->_inventory->_pouch < 30)
	{
	  my_putstr("Rupees insuffisants\n");
	  return ;
	}
      bowser_get()->_inventory->_pouch -= 30;
      inventory_add(IT_MUSHROOM, 1);
      my_putstr("Champignons acheté\n");
    }
  else
    {
      if (bowser_get()->_inventory->_pouch < 90)
	{
	  my_putstr("Rupees insuffisants\n");
	  return ;
	}
      bowser_get()->_inventory->_pouch -= 90;
      inventory_add(IT_MAGICBOX, 1);
      my_putstr("Magic box acheté\n");
    }
}
