/*
** Bowser.c for Midgard in /home/jon/Workspace/ETNA/Midgard/introduction
** 
** Made by Manh-Tuong NGUYEN
** Login   <nguyen_m@etna-alternance.net>
** 
** Started on  Tue Dec  6 10:20:59 2016 Manh-Tuong NGUYEN
** Last update Fri Dec  9 08:54:42 2016 Manh-Tuong NGUYEN
*/

#include <stdlib.h>

#include "libmy.h"
#include "Command.h"
#include "Bowser.h"
#include "Creature.h"

static t_bowser		*mr_b = NULL;
static int		creature_ai = 0;

int	bowser_init(char const *name)
{
  if ((mr_b = malloc(sizeof(t_bowser))) == NULL)
    return (1);
  mr_b->_name = my_strdup(name);
  mr_b->_team_size = 0;
  mr_b->_idfight = -1;
  mr_b->_team = NULL;
  mr_b->_cmdlist = NULL;
  mr_b->_inventory = NULL;
  inventory_init();
  mr_b->_partyloop = 1;
  return (0);
}


void		bowser_free()
{
  t_team	*buff;

  free(mr_b->_name);
  command_free_all();
  while (mr_b->_team)
    {
      buff = mr_b->_team->_next;
       free(mr_b->_team->_creature->name);
       free(mr_b->_team->_creature);
       free(mr_b->_team);
       mr_b->_team = buff;
    }
  creature_free();
  inventory_free();
  free(mr_b);
  mr_b = NULL;
}

t_bowser	*bowser_get()
{
  return (mr_b);
}

int		team_add_creature(t_creature *newc)
{
  t_team	*newt;

  if ((newt = malloc(sizeof(t_team))) == NULL)
    return (1);
  ++(mr_b->_team_size);
  newt->_id = creature_ai++;
  newt->_creature = newc;
  newt->_creature->pvmax *= 2;
  newt->_creature->pmmax *= 2;
  newt->_creature->pv = newt->_creature->pvmax;
  newt->_creature->pm = newt->_creature->pmmax;
  newt->_prev = NULL;
  newt->_next = mr_b->_team;
  mr_b->_team = newt;
  return (0);
}

void		team_delete_creature(t_creature *crea)
{
  t_team	*team;

  team = mr_b->_team;
  while (team && team->_creature != crea)
    team = team->_next;
  if (!team)
    return ;
  if (team->_prev)
    team->_prev->_next = team->_next;
  else
    mr_b->_team = team->_next;
  if (team->_next)
    team->_next->_prev = team->_prev;
  --(mr_b->_team_size);
  if (mr_b->_idfight == team->_id)
    mr_b->_idfight = -1;
  free(team->_creature->name);
  free(team->_creature);
  free(team);
}
