/*
** Bowser.h for Midgard in /home/jon/Workspace/ETNA/Midgard/introduction
** 
** Made by Manh-Tuong NGUYEN
** Login   <nguyen_m@etna-alternance.net>
** 
** Started on  Tue Dec  6 10:15:43 2016 Manh-Tuong NGUYEN
** Last update Fri Dec  9 08:53:36 2016 Manh-Tuong NGUYEN
*/

#include "Creature.h"
#include "Command.h"
#include "Inventory.h"

#ifndef MIDGARDBATTLE_BOWSER_HPP_
# define MIDGARDBATTLE_BOWSER_HPP_

/*
** Team double chained list
*/
typedef	struct	s_team
{
  int		_id;
  t_creature	*_creature;
  struct s_team	*_prev;
  struct s_team	*_next;
}		t_team;

/*
** Bowser (user) struct typedef
*/
typedef	struct	s_bowser
{
  char		*_name;
  int		_team_size;
  int		_idfight;
  t_team	*_team;
  t_command	*_cmdlist;
  t_inventory	*_inventory;
  int		_partyloop;
}		t_bowser;

/*
** Bowser.c
*/
int		bowser_init(char const *);
void		bowser_free();
t_bowser	*bowser_get();
int		team_add_creature(t_creature *);
void		team_delete_creature(t_creature *);

#endif /* !MIDGARDBATTLE_BOWSER_HPP_ */
