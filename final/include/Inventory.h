/*
** Inventory.h for Midgard in /home/jon/Workspace/ETNA/Midgard/introduction
** 
** Made by Manh-Tuong NGUYEN
** Login   <nguyen_m@etna-alternance.net>
** 
** Started on  Tue Dec  6 13:35:07 2016 Manh-Tuong NGUYEN
** Last update Thu Dec  8 20:29:54 2016 NOMENTSOA Cedric
*/

#ifndef MIDGARDBATTLE_INVENTORY_HPP_
# define MIDGARDBATTLE_INVENTORY_HPP_

/*
** Items enum
*/
typedef enum	e_item
  {
    IT_MUSHROOM,
    IT_MAGICBOX
  }		t_item;

/*
** Item chained list element
*/
typedef struct		s_itemnode
{
  t_item		_item;
  unsigned int		_nb;
  struct s_itemnode	*_prev;
  struct s_itemnode	*_next;
}			t_itemnode;

/*
** Inventory struct typedef
*/
typedef struct	s_inventory
{
  unsigned int	_pouch;
  t_itemnode	*_itemlist;
}		t_inventory;


/*
** Inventory.c
*/
int		inventory_init();
t_itemnode	*inventory_has(t_item);
int		inventory_add(t_item, unsigned int);
void		inventory_delete(t_item, unsigned int);
void		inventory_free();

#endif /* !MIDGARDBATTLE_INVENTORY_HPP_ */
