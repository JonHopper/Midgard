/*
** io.c for LibMy in /home/nguyenm/Piscine/C/LibMy
** 
** Made by NGUYEN Manh-Tuong
** Login   <nguyen_m@etna-alternance.net>
** 
** Started on  Fri Oct 21 10:21:23 2016 NGUYEN Manh-Tuong
** Last update Wed Dec  7 11:12:03 2016 NOMENTSOA Cedric
*/

#include <unistd.h>

void	my_putchar(char c)
{
  write(1, &c, 1);
}

void    my_putstr(char const *str)
{
  int	len;

  len = -1;
  while (str[++len] != '\0');
  write(1, str, len);
}

void	pos(long n)
{
  char	c[100];
  long	save;
  long	i;

  i = 0;
  save = n;
  while (save > 0)
    {
      save = save / 10;
      i++;
    }
  c[i] = '\0';
  i--;
  while (i >= 0)
    {
      c[i] = (n % 10) + 48;
      n = n / 10;
      i--;
    }
  my_putstr(c);
}

void	neg(long n)
{
  char	c[100];
  long	save;
  long	i;

  i = 0;
  n = n * (-1);
  save = n;
  while (save > 0)
    {
      save = save / 10;
      i++;
    }
  c[i + 1] = '\0';
  while (i > 0)
    {
      c[i] = (n % 10) + 48;
      n = n / 10;
      i--;
    }
  c[i] = '-';
  my_putstr(c);
}

void	my_put_nbr(int n)
{
  if (n == 0)
    my_putchar('0');
  else if (n > 0)
    pos(n);
  else
    neg(n);
}
