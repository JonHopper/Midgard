/*
** Cmd_fct.c for Cmd_fct.c in /home/cedric/Documents/midgard/introduction
** 
** Made by NOMENTSOA Cedric
** Login   <noment_c@etna-alternance.net>
** 
** Started on  Tue Dec  6 14:15:55 2016 NOMENTSOA Cedric
** Last update Wed Dec  7 11:18:10 2016 NOMENTSOA Cedric
*/

#include <stdlib.h>
#include <time.h>

#include "Bowser.h"
#include "libmy.h"
#include "Creature.h"
#include "Arg.h"

int	magic_catch()
{
  int	rnd;

  rnd = rand() % 3;
  if (rnd < 1)
    {
      creature_info();
      if (team_add_creature(creature_dup(bowser_get()->_creature)))
	return (1);
      my_putstr("Créature rajouté à votre team\n");
      bowser_get()->_partyloop = 0;
    }
  else
    {
      my_putstr("Raté, la créature vous charge !!\nVous fuyez\n");
      creature_free();
      if ((bowser_get()->_creature = creature_get()) == NULL)
	return (1);
    }
  return (0);
}

int	help_me()
{
  my_putstr("Vous fuyez lachement\n");
  creature_free();
  if ((bowser_get()->_creature = creature_get()) == NULL)
    return (1);
  return (0);
}

int	quit()
{
  bowser_get()->_partyloop = 0;
  return (0);
}

void		party_loop()
{
  char		*entry;
  t_bowser	*mr_b;
  t_command	*buff;

  mr_b = bowser_get();
  while (mr_b->_partyloop)
    {
      buff = mr_b->_cmdlist;
      my_putstr("Votre tour> ");
      if ((entry = readLine()) == NULL)
	return ;
      if (my_strlen(entry) == 0)
	return ;
      while (buff != NULL && my_strcmp(buff->_cmd, entry))
	buff = buff->_next;
      free(entry);
      if (buff == NULL)
	my_putstr("Commande inconnue\n");
      else
	buff->_func();
    }
}
