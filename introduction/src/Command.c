/*
** Command.c for Midgard in /home/jon/Workspace/ETNA/Midgard/introduction
** 
** Made by Manh-Tuong NGUYEN
** Login   <nguyen_m@etna-alternance.net>
** 
** Started on  Tue Dec  6 10:30:51 2016 Manh-Tuong NGUYEN
** Last update Wed Dec  7 11:23:45 2016 NOMENTSOA Cedric
*/

#include <stdlib.h>

#include "libmy.h"
#include "Command.h"
#include "Bowser.h"
#include "Cmd_fct.h"

int		command_init()
{
  if (command_add(magic_catch, "magic catch"))
    return (1);
  if (command_add(help_me, "help me !!!"))
    return (1);
  if (command_add(quit, "quit"))
    return (1);
  return (0);
}

int		command_add(t_cmdptr func, char const *name)
{
  t_command	*newc;
  t_bowser	*mr_b;

  if ((newc = malloc(sizeof(t_command))) == NULL)
    return (1);
  if ((newc->_cmd = my_strdup(name)) == NULL)
    return (1);
  newc->_func = func;
  mr_b = bowser_get();
  if (mr_b->_cmdlist == NULL)
    newc->_next = NULL;
  else
    newc->_next = mr_b->_cmdlist;
  mr_b->_cmdlist = newc;
  return (0);
}

void		command_delete(char const *name)
{
  t_command	*buff;
  t_command	*tmp;

  if ((buff = bowser_get()->_cmdlist) == NULL)
    return ;
  if (my_strcmp(buff->_cmd, name) == 0)
    {
      tmp = buff->_next;
      free(buff->_cmd);
      free(buff);
      bowser_get()->_cmdlist = tmp;
    }
  while (buff->_next || my_strcmp(buff->_next->_cmd, name) != 0)
    buff = buff->_next;
  if (!buff->_next)
    return ;
  tmp = buff->_next->_next;
  free(buff->_next->_cmd);
  free(buff->_next);
  buff->_next = tmp;
}

void		command_free_all()
{
  t_bowser	*mr_b;
  t_command	*buff;

  if ((mr_b = bowser_get()) == NULL)
    return ;
  if (!mr_b->_cmdlist)
    return ;
  while (mr_b->_cmdlist)
    {
      buff = mr_b->_cmdlist->_next;
      free(mr_b->_cmdlist->_cmd);
      free(mr_b->_cmdlist);
      mr_b->_cmdlist = buff;
    }
}
