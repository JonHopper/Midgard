/*
** Creature.c for Midgard in /home/jon/Workspace/ETNA/Midgard/introduction
** 
** Made by Manh-Tuong NGUYEN
** Login   <nguyen_m@etna-alternance.net>
** 
** Started on  Tue Dec  6 13:12:01 2016 Manh-Tuong NGUYEN
** Last update Wed Dec  7 11:24:59 2016 NOMENTSOA Cedric
*/

#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "libmy.h"
#include "Creature.h"
#include "Bowser.h"

# define NBCREA	5
static t_creature g_creatures[] =
  {
    {"Koopa", 1, 10, 10, 20, 20},
    {"Bob bomb", 1, 10, 10, 20, 20},
    {"Yoshi", 1, 10, 10, 20, 20},
    {"Maskas", 1, 10, 10, 20, 20},
    {"Kucco", 1, 10, 10, 20, 20},
    {NULL, 0, 0, 0, 0, 0}
  };


t_creature	*creature_get()
{
  int		rnd;
  t_creature	*crea;

  rnd = rand() % NBCREA;
  if ((crea = malloc(sizeof(t_creature))) == NULL)
    return (NULL);
  crea->name = my_strdup(g_creatures[rnd].name);
  if (!crea->name)
    return (NULL);
  crea->lvl = g_creatures[rnd].lvl;
  crea->pv = g_creatures[rnd].pv;
  crea->pvmax = g_creatures[rnd].pvmax;
  crea->pm = g_creatures[rnd].pm;
  crea->pmmax = g_creatures[rnd].pmmax;
  my_putstr("Un ");
  my_putstr(crea->name);
  my_putstr(" a pop !\n");
  return (crea);
}

t_creature	*creature_dup(t_creature *crea_src)
{
  t_creature	*crea;

  if ((crea = malloc(sizeof(t_creature))) == NULL)
    return (NULL);
  crea->name = my_strdup(crea_src->name);
  if (!crea->name)
    return (NULL);
  crea->lvl = crea_src->lvl;
  crea->pv = crea_src->pvmax;
  crea->pvmax = crea_src->pvmax;
  crea->pm = crea_src->pmmax;
  crea->pmmax = crea_src->pmmax;
  return (crea);
}

void		creature_info()
{
  t_bowser	*mr_b;

  mr_b = bowser_get();
  my_putstr(mr_b->_creature->name);
  my_putstr(" capturé\nNiveau : ");
  my_put_nbr(mr_b->_creature->lvl);
  my_putstr("\nPV : ");
  my_put_nbr(mr_b->_creature->pvmax);
  my_putstr("\nPM : ");
  my_put_nbr(mr_b->_creature->pmmax);
  my_putstr("\n");
}

void		creature_free()
{
  t_bowser	*mr_b;

  mr_b = bowser_get();
  if (mr_b->_creature == NULL)
    return ;
  free(mr_b->_creature->name);
  free(mr_b->_creature);
  mr_b->_creature = NULL;
}
