/*
** Bowser.c for Midgard in /home/jon/Workspace/ETNA/Midgard/introduction
** 
** Made by Manh-Tuong NGUYEN
** Login   <nguyen_m@etna-alternance.net>
** 
** Started on  Tue Dec  6 10:20:59 2016 Manh-Tuong NGUYEN
** Last update Wed Dec  7 11:07:26 2016 NOMENTSOA Cedric
*/

#include <stdlib.h>

#include "libmy.h"
#include "Command.h"
#include "Bowser.h"
#include "Creature.h"

static t_bowser		*mr_b = NULL;

int	bowser_init(char const *name)
{
  if ((mr_b = malloc(sizeof(t_bowser))) == NULL)
    return (1);
  mr_b->_name = my_strdup(name);
  mr_b->_team = NULL;
  mr_b->_cmdlist = NULL;
  if ((mr_b->_creature = creature_get()) == NULL)
    return (1);
  mr_b->_partyloop = 1;
  return (0);
}


void	bowser_free()
{
  free(mr_b->_name);
  command_free_all();
  team_free_all();
  creature_free();
  free(mr_b);
  mr_b = NULL;
}

t_bowser	*bowser_get()
{
  return (mr_b);
}

int		team_add_creature(t_creature *newc)
{
  t_team	*newt;

  if ((newt = malloc(sizeof(t_team))) == NULL)
    return (1);
  newt->_creature = newc;
  newt->_prev = NULL;
  newt->_next = mr_b->_team;
  mr_b->_team = newt;
  return (0);
}

void		team_free_all()
{
  t_team	*buff;

  while (mr_b->_team)
    {
      buff = mr_b->_team->_next;
      free(mr_b->_team->_creature->name);
      free(mr_b->_team->_creature);
      free(mr_b->_team);
      mr_b->_team = buff;
    }
}
