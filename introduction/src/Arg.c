/*
** Arg.c for Arg.c in /home/cedric/Documents/midgard/introduction
** 
** Made by NOMENTSOA Cedric
** Login   <noment_c@etna-alternance.net>
** 
** Started on  Tue Dec  6 13:43:21 2016 NOMENTSOA Cedric
** Last update Wed Dec  7 11:15:41 2016 NOMENTSOA Cedric
*/

#include <stdlib.h>
#include <unistd.h>

#include "libmy.h"

char		*readLine()
{
  ssize_t	ret;
  char		*buff;

  if ((buff = malloc(sizeof(*buff) * (50 + 1))) == NULL)
    return (NULL);
  if ((ret = read(0, buff, 50)) > -1)
    {
      if (ret > 0)
	buff[ret - 1] = '\0';
      else
	buff[0] = '\0';
      return (buff);
    }
  free(buff);
  return (NULL);
}

int	catch_arg(int argc, char *argv[])
{
  if (argc != 3 || my_strcmp(argv[1], "-n"))
    {
      my_putstr("Erreur argument : ./str -n [name]\n");
      return (1);
    }
  return (0);
}
