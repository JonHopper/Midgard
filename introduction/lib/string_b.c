/*
** string_b.c for LibMy in /home/nguyenm/Piscine/C/LibMy
** 
** Made by NGUYEN Manh-Tuong
** Login   <nguyen_m@etna-alternance.net>
** 
** Started on  Fri Oct 21 11:51:43 2016 NGUYEN Manh-Tuong
** Last update Tue Dec  6 11:41:49 2016 Jonathan S. Hopper
*/

#include <stdlib.h>

char    *my_strcat(char *str1, char const *str2)
{
  int	i;
  int	start;

  start = -1;
  while (str1[++start] != '\0');
  i = -1;
  while (str2[++i] != '\0')
    str1[start + i] = str2[i];
  str1[start + i] = '\0';
  return (str1);
}

char    *my_strncat(char *str1, char const *str2, int n)
{
  int	i;
  int	start;

  start = -1;
  while (str1[++start] != '\0');
  i = -1;
  while (str2[++i] != '\0' && i < n)
    str1[start + i] = str2[i];
  str1[start + i] = '\0';
  return (str1);
}

char	*my_strstr(char *str, char const *to_find)
{
  int	i[2];

  i[0] = -1;
  i[1] = 0;
  while (str[++i[0]] != '\0')
    {
      if (str[i[0]] == to_find[i[1]])
	++i[1];
      else if (to_find[i[1]] == '\0')
	return (&(str[i[0] - i[1]]));
      else
	i[1] = 0;
    }
  return (NULL);
}

