/*
** string_c.c for LibMy in /home/nguyenm/Piscine/C/LibMy/libmy_02
** 
** Made by NGUYEN Manh-Tuong
** Login   <nguyen_m@etna-alternance.net>
** 
** Started on  Mon Oct 24 09:51:36 2016 NGUYEN Manh-Tuong
** Last update Tue Dec  6 11:39:03 2016 Jonathan S. Hopper
*/

#include <stdlib.h>

char	*my_strdup(char const *str)
{
  int	len;
  char	*dup;

  if (str == NULL)
    return (NULL);
  len = -1;
  while (str[++len] != '\0');
  if ((dup = malloc((len + 1) * sizeof(char))) == NULL)
    return (NULL);
  len = -1;
  while (str[++len] != '\0')
    dup[len] = str[len];
  dup[len] = '\0';
  return (dup);
}

int	my_str_countword(char const *str)
{
  int	i;
  int	word;
  int	count;

  i = -1;
  word = 0;
  count = 0;
  while (str[++i] != '\0')
    {
      if ((str[i] >= 'a' && str[i] <= 'z') || (str[i] >= 'A' && str[i] <= 'Z')
	  || (str[i] >= '0' && str[i] <= '9'))
	{
	  if (word == 0)
	    ++count;
	  word = 1;
	}
      else
	word = 0;
    }
  return (count);
}

char	*my_str_getword(char const *str, int *i)
{
  int	idx;
  int	start;
  char	*word;

  start = *i;
  while (str[*i] != '\0' && ((str[*i] >= 'a' && str[*i] <= 'z') ||
	  (str[*i] >= 'A' && str[*i] <= 'Z') || (str[*i] >= '0' && str[*i] <= '9')))
    ++(*i);
  if ((word = malloc((*i - start + 1) * sizeof(char))) == NULL)
    return (NULL);
  idx = -1;
  while ((start + ++idx) < *i)
    word[idx] = str[start + idx];
  word[idx] = '\0';
  return (word);
}

char	**my_str_to_wordtab(char const *str)
{
  int	i;
  int	wordcount;
  int	wordnb;
  char	**wordtab;

  wordcount = my_str_countword(str);
  if ((wordtab = malloc((wordcount + 1) * sizeof(char *))) == NULL)
    return (NULL);
  i = 0;
  wordnb = -1;
  while (wordnb < wordcount)
    {
      if ((str[i] >= 'a' && str[i] <= 'z') || (str[i] >= 'A' && str[i] <= 'Z')
	  || (str[i] >= '0' && str[i] <= '9'))
	wordtab[++wordnb] = my_str_getword(str, &i);
      ++i;
    }
  wordtab[wordnb] = NULL;
  return (wordtab);
}
