/*
** integer.c for MyLib in /home/nguyenm/Piscine/C/LibMy
** 
** Made by NGUYEN Manh-Tuong
** Login   <nguyen_m@etna-alternance.net>
** 
** Started on  Fri Oct 21 10:27:17 2016 NGUYEN Manh-Tuong
** Last update Fri Oct 21 10:30:52 2016 NGUYEN Manh-Tuong
*/

int     my_isneg(int n)
{
  if (n < 0)
    return (0);
  return (1);
}

void    my_swap(int *a, int *b)
{
  int	tmp;

  tmp = *a;
  *a = *b;
  *b = tmp;
}
