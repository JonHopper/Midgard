/*
** Cmd_fct.h for Cmd_fct.h in /home/cedric/Documents/midgard/introduction
** 
** Made by NOMENTSOA Cedric
** Login   <noment_c@etna-alternance.net>
** 
** Started on  Tue Dec  6 14:16:30 2016 NOMENTSOA Cedric
** Last update Tue Dec  6 14:56:29 2016 NOMENTSOA Cedric
*/

#ifndef _CMD_FCT_H
# define _CMD_FCT_H

int	magic_catch();
int	help_me();
int	quit();
void	party_loop();

#endif /* !_CMD_FCT_H */
