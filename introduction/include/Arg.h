/*
** Arg.h for Arg.h in /home/cedric/Documents/midgard/introduction
** 
** Made by NOMENTSOA Cedric
** Login   <noment_c@etna-alternance.net>
** 
** Started on  Tue Dec  6 13:40:31 2016 NOMENTSOA Cedric
** Last update Wed Dec  7 11:16:08 2016 NOMENTSOA Cedric
*/

#ifndef _ARG_H_
# define _ARG_H_

int	catch_arg(int argc, char *argv[]);
char	*readLine();

#endif /* !_ARG_H_ */
