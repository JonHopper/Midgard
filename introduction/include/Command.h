/*
** Command.h for Midgard in /home/jon/Workspace/ETNA/Midgard/introduction
** 
** Made by Manh-Tuong NGUYEN
** Login   <nguyen_m@etna-alternance.net>
** 
** Started on  Tue Dec  6 10:25:46 2016 Manh-Tuong NGUYEN
** Last update Tue Dec  6 14:44:37 2016 NOMENTSOA Cedric
*/

#ifndef MIDGARDBATTLE_COMMAND_HPP_
# define MIDGARDBATTLE_COMMAND_HPP_

/*
** Command function pointer typedef
*/
typedef		int (*t_cmdptr)();

/*
** Command chained list
*/
typedef	struct		s_command
{
  char			*_cmd;
  t_cmdptr		_func;
  struct s_command	*_next;
}			t_command;

/*
** Command.c
*/
int	command_init();
int	command_add(t_cmdptr, char const *);
void	command_delete(char const *);
void	command_free_all();

#endif /*! MIDGARDBATTLE_COMMAND_HPP_ */
