/*
** Creature.h for Midgard in /home/jon/Workspace/ETNA/Midgard/introduction
** 
** Made by Manh-Tuong NGUYEN
** Login   <nguyen_m@etna-alternance.net>
** 
** Started on  Tue Dec  6 10:19:26 2016 Manh-Tuong NGUYEN
** Last update Wed Dec  7 10:56:19 2016 NOMENTSOA Cedric
*/

#ifndef MIDGARDBATTLE_CREATURE_HPP_
# define MIDGARDBATTLE_CREATURE_HPP_

/*
** Creature struct typedef
*/
typedef struct  s_creature
{
  char		*name;
  int		lvl;
  int		pv;
  int		pvmax;
  int		pm;
  int		pmmax;
}		t_creature;

/*
** Creature.c
*/
t_creature	*creature_get();
t_creature	*creature_dup(t_creature *);
void		creature_info();
void		creature_free();

#endif /* !MIDGARDBATTLE_CREATURE_HPP_ */
