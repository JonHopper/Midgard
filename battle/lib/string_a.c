/*
** string_a.c for MyLib in /home/nguyenm/Piscine/C/LibMy
** 
** Made by NGUYEN Manh-Tuong
** Login   <nguyen_m@etna-alternance.net>
** 
** Started on  Fri Oct 21 10:31:04 2016 NGUYEN Manh-Tuong
** Last update Tue Dec  6 11:40:35 2016 Jonathan S. Hopper
*/

int     my_strlen(char const *str)
{
  int	len;

  len = -1;
  while (str[++len] != '\0');
  return (len);
}

char	*my_strcpy(char *dest, char const *src)
{
  int	i;

  if (src == (void *)0)
    return (dest);;
  i = -1;
  while (src[++i] != '\0')
    dest[i] = src[i];
  dest[i] = '\0';
  return (dest);
}

char	*my_strncpy(char *dest, char const *src, int n)
{
  int	i;
  int	len;

  if (src == (void *)0)
    return (dest);
  len = my_strlen(src);
  i = -1;
  while (++i < n)
    {
      if (i < len)
	dest[i] = src[i];
      else
	dest[i] = '\0';
    }
  if (n >= len)
    dest[i] = '\0';
  return (dest);
}

int	my_strcmp(char const *s1, char const *s2)
{
  int	i;
  int	len[2];

  len[0] = my_strlen(s1);
  len[1] = my_strlen(s2);
  if (len[0] < len[1])
    return (-1);
  if (len[1] > len[1])
    return (1);
  i = -1;
  while (++i < len[0])
    {
      if (s1[i] < s2[i])
	return (-1);
      if (s1[i] > s2[i])
	return (1);
    }
  return (0);
}

int	my_strncmp(char const *s1, char const *s2, int n)
{
  int	i;

  i = -1;
  while (s1[++i] != '\0' && i < n)
    {
      if (s1[i] < s2[i])
	return (-1);
      if (s1[i] > s2[i])
	return (1);
    }
  return (0);
}
