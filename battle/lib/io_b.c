/*
** io_b.c for io_a.c in /home/cedric/Documents/midgard/introduction
** 
** Made by NOMENTSOA Cedric
** Login   <noment_c@etna-alternance.net>
** 
** Started on  Wed Dec  7 11:12:52 2016 NOMENTSOA Cedric
** Last update Wed Dec  7 11:13:11 2016 NOMENTSOA Cedric
*/

#include <unistd.h>

int	my_get_number(char const *str, int siz)
{
  int	tenpow;
  int	nbr;

  tenpow = 1;
  nbr = 0;
  while (--siz >= 0)
    {
      nbr += (str[siz] - '0') * tenpow;
      tenpow *= 10;
    }
  return (nbr);
}

int	my_getnbr(char const *str)
{
  int	i;
  int	neg;
  int	start;

  i = -1;
  neg = 0;
  while (str[++i] != '\0')
    {
      if (str[i] == '-')
	neg += 1;
      else if (str[i] >= '0' && str[i] <= '9')
	break ;
      else if (str[i] != '+')
	return (0);
    }
  start = i--;
  while (str[++i] != '\0' && str[i] >= '0' && str[i] <= '9');
  if ((neg % 2) != 0)
    return (-my_get_number(str + start, i - start));
  return (my_get_number(str + start, i - start));
}
