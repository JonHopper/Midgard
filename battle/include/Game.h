/*
** Game.h for Midgard in /home/jon/Workspace/ETNA/Midgard/battle
** 
** Made by Manh-Tuong NGUYEN
** Login   <nguyen_m@etna-alternance.net>
** 
** Started on  Wed Dec  7 15:52:18 2016 Manh-Tuong NGUYEN
** Last update Thu Dec  8 10:35:16 2016 NOMENTSOA Cedric
*/

#ifndef MIDGARDBATTLE_GAME_H_
# define MIDGARDBATTLE_GAME_H_

/*
** Game state enum
*/
typedef enum	e_gamestate
  {
    GS_OUTOFBATTLE,
    GS_INBATTLE
  }		t_gamestate;

/*
** Game.c
*/
char		*readLine();
void		party_loop();
void		execute_command(char const *);
t_gamestate	game_getstate();
void		game_setstate(t_gamestate state);

#endif /* !MIDGARDBATTLE_GAME_H_ */
