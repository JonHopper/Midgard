/*
** Arg.h for Arg.h in /home/cedric/Documents/midgard/introduction
** 
** Made by NOMENTSOA Cedric
** Login   <noment_c@etna-alternance.net>
** 
** Started on  Tue Dec  6 13:40:31 2016 NOMENTSOA Cedric
** Last update Thu Dec  8 19:19:34 2016 NOMENTSOA Cedric
*/

#ifndef _ARG_H_
# define _ARG_H_

int	catch_arg(int argc, char *argv[]);
int	quit();

#endif /* !_ARG_H_ */
