/*
** Creature.h for Midgard in /home/jon/Workspace/ETNA/Midgard/introduction
** 
** Made by Manh-Tuong NGUYEN
** Login   <nguyen_m@etna-alternance.net>
** 
** Started on  Tue Dec  6 10:19:26 2016 Manh-Tuong NGUYEN
** Last update Thu Dec  8 10:49:02 2016 Manh-Tuong NGUYEN
*/

#ifndef MIDGARDBATTLE_CREATURE_HPP_
# define MIDGARDBATTLE_CREATURE_HPP_

/*
** Creature struct typedef
*/
typedef struct  s_creature
{
  char		*name;
  int		lvl;
  int		pv;
  int		pvmax;
  int		pm;
  int		pmmax;
}		t_creature;

/*
** Creature.c
*/
int		creature_spawn();
t_creature	*creature_get();
t_creature	*creature_dup();
void		creature_info();
void		creature_free();

#endif /* !MIDGARDBATTLE_CREATURE_HPP_ */
