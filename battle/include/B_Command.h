/*
** B_Command.h for Midgard
** 
** Made by NOMENTSOA Cedric
** Login   <noment_c@etna-alternance.net>
** 
** Started on  Tue Dec  6 14:16:30 2016 NOMENTSOA Cedric
** Last update Thu Dec  8 18:59:43 2016 NOMENTSOA Cedric
*/

#ifndef MIDGARDBATTLE_IB_COMMAND_H_
# define MIDGARDBATTLE_IB_COMMAND_H_

/*
** IB_Command.c
*/
int	magic_catch();
int	help_me();
int	check_creature();
int	check_creature_ennemy();
void	ennemy_attack();

/*
** IB_Command2.c
*/
int		slash();
int		fire();
int		gamble();
int		rest();
t_creature	*choose_creature();

/*
** OoB_Command.c
*/
int	team_info();
int	choose_one();
int	fight();

#endif /* !MIDGARDBATTLE_IB_COMMAND_H_ */
