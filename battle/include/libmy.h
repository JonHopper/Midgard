/*
** libmy.h for LibMy in /home/nguyenm/Piscine/C/LibMy
** 
** Made by Manh-Tuong NGUYEN
** Login   <nguyen_m@etna-alternance.net>
** 
** Started on  Fri Oct 21 10:22:16 2016 Manh-Tuong NGUYEN
** Last update Tue Dec  6 13:13:06 2016 Manh-Tuong NGUYEN
*/

#ifndef LIBMY_H_
# define LIBMY_H_

/*
** io.c
*/
void    my_putchar(char c);
void    my_putstr(char const *str);
void    my_put_nbr(int nb);
int     my_getnbr(char const *str);

/*
** integer.c
*/
int     my_isneg(int n);
void    my_swap(int *a, int *b);

/*
** string_a.c
*/
int     my_strlen(char const *str);
char    *my_strcpy(char *dest, char const *src);
char    *my_strncpy(char *dest, char const *src, int n);
int     my_strcmp(char const *s1, char const *s2);
int     my_strncmp(char const *s1, char const *s2, int n);

/*
** string_b.c
*/
char    *my_strcat(char *str1, char const *str2);
char    *my_strncat(char *str1, char const *str2, int n);
char    *my_strstr(char *str, char const *to_find);

/*
** string_c.c
*/
char	*my_strdup(char const *str);
char	**my_str_to_wordtab(char const *str);

#endif /* !LIBMY_H_ */
