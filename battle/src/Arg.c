/*
** Arg.c for Arg.c in /home/cedric/Documents/midgard/introduction
** 
** Made by NOMENTSOA Cedric
** Login   <noment_c@etna-alternance.net>
** 
** Started on  Tue Dec  6 13:43:21 2016 NOMENTSOA Cedric
** Last update Fri Dec  9 14:47:22 2016 NOMENTSOA Cedric
*/

#include "libmy.h"
#include "Game.h"
#include "Bowser.h"
#include "Arg.h"
#include "Creature.h"

int	catch_arg(int argc, char *argv[])
{
  if (argc != 3 || my_strcmp(argv[1], "-n"))
    {
      my_putstr("Erreur argument : ./str -n [name]\n");
      return (1);
    }
  return (0);
}

int	quit()
{
  creature_free();
  game_setstate(GS_OUTOFBATTLE);
  heal_creature();
  return (0);
}
