/*
** IB_Command.c for Midgard
** 
** Made by NOMENTSOA Cedric
** Login   <noment_c@etna-alternance.net>
** 
** Started on  Tue Dec  6 14:15:55 2016 NOMENTSOA Cedric
** Last update Thu Dec  8 19:10:25 2016 NOMENTSOA Cedric
*/

#include <stdlib.h>
#include <time.h>

#include "Bowser.h"
#include "libmy.h"
#include "Creature.h"
#include "Game.h"
#include "B_Command.h"

int	magic_catch()
{
  int	rnd;

  rnd = rand() % 3;
  if (rnd < 1)
    {
      creature_info();
      if (team_add_creature(creature_dup()))
	return (1);
      my_putstr("Créature rajouté à votre team\n");
      creature_free();
      game_setstate(GS_OUTOFBATTLE);
      heal_creature();
    }
  else
    {
      my_putstr("Capture raté\n");
      ennemy_attack();
      if (check_creature())
	return (1);
    }
  return (0);
}

int	help_me()
{
  my_putstr("Vous fuyez lachement\n");
  creature_free();
  return (creature_spawn());
}

int	check_creature()
{
  if (choose_creature()->pv <= 0)
    {
      my_putstr("Votre créature est morte\n");
      game_setstate(GS_OUTOFBATTLE);
      heal_creature();
      return (1);
    }
  return (0);
}

int	check_creature_ennemy()
{
  if (creature_get()->pv <= 0)
    {
      my_putstr("Créature tué\n");
      creature_free();
      game_setstate(GS_OUTOFBATTLE);
      heal_creature();
      return (1);
    }
  return (0);
}

void	ennemy_attack()
{
  int	rnd;

  rnd = rand() % 4;
  if (rnd == 0)
    {
      my_putstr("L'ennemi vous slash : 15 points de dégats\n");
      choose_creature()->pv -= 15;
    }
  else if (rnd == 1)
    {
      my_putstr("L'ennemi vous fire : 30 points de dégats\n");
      choose_creature()->pv -= 30;
    }
  else if (rnd == 2)
    {
      my_putstr("L'ennemi gamble\n");
      gamble();
    }
  else
    {
      my_putstr("L'ennemi se repose\n");
      creature_get()->pm += 10;
    }
}
