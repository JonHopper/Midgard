/*
** IB_Command2.c for Midgard in /home/jon/Workspace/ETNA/Midgard/battle
** 
** Made by Manh-Tuong NGUYEN
** Login   <nguyen_m@etna-alternance.net>
** 
** Started on  Wed Dec  7 16:36:32 2016 Manh-Tuong NGUYEN
** Last update Thu Dec  8 19:26:16 2016 NOMENTSOA Cedric
*/

#include <time.h>
#include <stdlib.h>

#include "libmy.h"
#include "Creature.h"
#include "B_Command.h"
#include "Bowser.h"

int		slash()
{
  t_creature	*crea;

  crea = choose_creature();
  if (crea->pm < 3)
    {
      my_putstr("PM insuffisants\n");
      return (1);
    }
  crea->pm -= 3;
  creature_get()->pv -= 15;
  my_putstr("Vous infligez 15 points de dégats\n");
  if (check_creature_ennemy())
    return (1);
  ennemy_attack();
  if (check_creature())
    return (1);
  return (0);
}

int		fire()
{
  t_creature	*crea;

  crea = choose_creature();
  if (crea->pm < 7)
    {
      my_putstr("PM insuffisants\n");
      return (1);
    }
  crea->pm -= 7;
  creature_get()->pv -= 30;
  my_putstr("Vous infligez 30 points de dégats\n");
  if (check_creature_ennemy())
    return (1);
  ennemy_attack();
  if (check_creature())
    return (1);
  return (0);
}

int		gamble()
{
  int		rnd;
  t_creature	*crea;

  crea = choose_creature();
  rnd = rand() % 2;
  if (rnd)
    {
      rnd = rand() % 21;
      my_putstr("Perdu\nVous prenez ");
      my_put_nbr(rnd);
      my_putstr(" points de dégats\n");
      crea->pv -= rnd;
    }
  else
    {
      rnd = rand() % 21;
      my_putstr("Gagné\nL'ennemi perd ");
      my_put_nbr(rnd);
      my_putstr(" points de dégats\n");
      creature_get()->pv -= rnd;
    }
  if (check_creature_ennemy() || check_creature())
    return (1);
  return (0);
}

int		rest()
{
  t_creature	*crea;

  crea = choose_creature();
  crea->pm += 10;
  crea->pm = (crea->pm > crea->pmmax) ? crea->pmmax : crea->pm;
  my_putstr("Vous gagnez 10 points de PM\n");
  ennemy_attack();
  return (0);
}

t_creature	*choose_creature()
{
  t_team	*team;

  team = bowser_get()->_team;
  while (team && team->_id != bowser_get()->_idfight)
    team = team->_next;
  return (team->_creature);
}
