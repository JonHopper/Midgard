/*
** Bowser.c for Midgard in /home/jon/Workspace/ETNA/Midgard/introduction
** 
** Made by Manh-Tuong NGUYEN
** Login   <nguyen_m@etna-alternance.net>
** 
** Started on  Tue Dec  6 10:20:59 2016 Manh-Tuong NGUYEN
** Last update Thu Dec  8 20:14:38 2016 NOMENTSOA Cedric
*/

#include <stdlib.h>

#include "libmy.h"
#include "Command.h"
#include "Bowser.h"
#include "Creature.h"

static t_bowser		*mr_b = NULL;

int	bowser_init(char const *name)
{
  if ((mr_b = malloc(sizeof(t_bowser))) == NULL)
    return (1);
  mr_b->_name = my_strdup(name);
  mr_b->_team_size = 0;
  mr_b->_idfight = -1;
  mr_b->_team = NULL;
  mr_b->_cmdlist = NULL;
  creature_spawn();
  if (team_add_creature(creature_dup()))
    return (1);
  creature_free();
  mr_b->_partyloop = 1;
  return (0);
}


void		bowser_free()
{
   t_team	*buff;

   free(mr_b->_name);
   command_free_all();
   while (mr_b->_team)
     {
       buff = mr_b->_team->_next;
       free(mr_b->_team->_creature->name);
       free(mr_b->_team->_creature);
       free(mr_b->_team);
       mr_b->_team = buff;
     }
   creature_free();
   free(mr_b);
   mr_b = NULL;
}

t_bowser	*bowser_get()
{
  return (mr_b);
}

int		team_add_creature(t_creature *newc)
{
  t_team	*newt;

  if ((newt = malloc(sizeof(t_team))) == NULL)
    return (1);
  newt->_id = (mr_b->_team_size)++;
  newt->_creature = newc;
  newt->_creature->pvmax *= 2;
  newt->_creature->pmmax *= 2;
  newt->_creature->pv = newt->_creature->pvmax;
  newt->_creature->pm = newt->_creature->pmmax;
  newt->_prev = NULL;
  newt->_next = mr_b->_team;
  mr_b->_team = newt;
  return (0);
}

void		heal_creature()
{
  t_team	*team;

  team = bowser_get()->_team;
  if (team == NULL)
    return ;
  while (team)
    {
      team->_creature->pv = team->_creature->pvmax;
      team->_creature->pm = team->_creature->pmmax;
      team = team->_next;
    }
  my_putstr("Votre équipe a été soigné\n");
}
